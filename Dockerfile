# Version: 0.0.1

FROM centos:centos6
MAINTAINER Salvador Rodriguez <salvador.rodriguez@utah.edu>

# Install packages
ENV REFRESHED_AT 2015-02-06
RUN yum -y update; yum clean all
RUN yum -y install epel-release; yum clean all
RUN yum install wget -y
RUN yum install tar -y
RUN yum install sudo -y
RUN yum install net-tools -y

# Install java
RUN wget --no-cookies \
         --no-check-certificate \
         --header "Cookie: oraclelicense=accept-securebackup-cookie" \
         "http://download.oracle.com/otn-pub/java/jdk/8u25-b17/jdk-8u25-linux-x64.rpm"

RUN rpm -Uvh jdk-8u25-linux-x64.rpm
RUN rm jdk-8u25-linux-x64.rpm
ENV JAVA_HOME /usr/java/jdk1.8.0_25

# Set java home	
RUN echo JAVA_HOME=/usr/java/jdk1.8.0_25 >> /etc/profile
RUN echo PATH=$JAVA_HOME/bin:$PATH:$HOME/bin >> /etc/profile
RUN echo export PATH JAVA_HOME >> /etc/profile
RUN source /etc/profile

# Install MySQL
ADD ./config_mysql.sh /config_mysql.sh
RUN chmod 755 /config_mysql.sh
RUN /config_mysql.sh
RUN rm config_mysql.sh
ADD ./my.cnf /etc/my.cnf
RUN chkconfig mysqld on

# Install Apelon
RUN wget http://sourceforge.net/projects/apelon-dts/files/dts-linux/3.5.2/apelon-dts-3.5.2.203-linux-mysql.tar.gz

RUN mkdir /usr/local/apelon
RUN tar -zxf apelon-dts-3.5.2.203-linux-mysql.tar.gz -C /usr/local/apelon/
RUN rm -f apelon-dts-3.5.2.203-linux-mysql.tar.gz
RUN cd /usr/local/apelon/bin; sh makeScriptsExecutable.sh

RUN /etc/init.d/mysqld restart; cd /usr/local/apelon/bin/kb/create; ./kbcreate.sh
RUN sed -i 's/.*requiretty$/#Defaults requiretty/' /etc/sudoers

# MySQL dump file, comment the following line if you dont have a mysql dump file
ADD dts_dump_file.sql /dts_dump_file.sql

# expose port
EXPOSE 8081
EXPOSE 6666

CMD ["/bin/bash"]

