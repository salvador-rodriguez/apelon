Build image :
sudo docker build -t="opencds/apelon:centos6" .

Create container :
sudo docker run --name apelon -i -t opencds/apelon:centos6

Start mysql :
service mysqld start

Restore mysql backup :
place your sql dump file (e.g., dts_dump_file.sql) in the same folder where Dockerfile is located. Inside of your container run the following :
mysql -u root -pmysqlPassword dts < dts_dump_file.sql

Start Apelon :
cd /usr/local/apelon/bin/browser; nohup ./runtomcat.sh start &
cd /usr/local/apelon/bin/server; ./StartApelonServer.sh 

apelon available on x.x.x.x:8081/dtstreebrowser
starting server on x.x.x.x:6666

dtstreebrowser
- select mysql, localhost, user: dts, password dts:, port: 3306, database: dts

Find out the ipaddress of the container:
sudo docker inspect apelon | grep IPAddress


