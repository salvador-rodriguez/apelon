#!/bin/bash

__mysql_config() {
echo "Running the mysql_config function."
yum -y erase mysql mysql-server
rm -rf /var/lib/mysql/ /etc/my.cnf
wget http://repo.mysql.com/mysql-community-release-el6-5.noarch.rpm
yum localinstall mysql-community-release-el6-*.noarch.rpm -y
yum install mysql-community-server -y
chown -R mysql:mysql /var/lib/mysql
service mysqld start
chkconfig mysqld on
chkconfig --list mysqld
sleep 10
}

__start_mysql() {
echo "Running the start_mysql function."
mysqladmin -u root password mysqlPassword
mysql -uroot -pmysqlPassword -e "CREATE DATABASE dts"
mysql -uroot -pmysqlPassword -e "CREATE USER 'dts'@'localhost' IDENTIFIED BY 'dts';"
mysql -uroot -pmysqlPassword -e "GRANT ALL PRIVILEGES ON *.* TO 'dts'@'localhost' WITH GRANT OPTION;"		 
killall mysqld
sleep 10
}

# Call all functions
__mysql_config
__start_mysql
